from django.conf.urls import include, url
from django.urls import path
from django.contrib import admin
from django.contrib.auth import views
from . import views

#url for app
urlpatterns = [
    path('', views.home, name='home'),
]
